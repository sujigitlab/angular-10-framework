import { Component, OnInit, Inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router,  RouterEvent,  NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  env = environment;
  isLanding = false;
  constructor(@Inject(Router) private router) {
    router.events.subscribe((event: RouterEvent) => {
      if ( event instanceof NavigationEnd) {
        console.log(this.router.url);
        this.isLanding = this.router.url.indexOf('landing') > 0;
      }
    });
   }

  ngOnInit(): void {
  }

}
