import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicLandingComponent } from './public-landing/public-landing.component';
import { PublicLandingContentComponent } from './public-landing-content/public-landing-content.component';
import { CommonModule } from '@angular/common';
// tslint:disable-next-line:max-line-length


const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', redirectTo: 'landing', pathMatch: 'full' },
      { path: 'landing', component: PublicLandingComponent,
      children: [
        // { path: '', component: RegPanelLayoutComponent},
          {path: 'pw-site-content', component: PublicLandingContentComponent},

                ]
        },

    ]
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicWebsiteRoutingModule { }
