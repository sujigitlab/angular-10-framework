import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicLandingContentComponent } from './public-landing-content.component';

describe('PublicLandingContentComponent', () => {
  let component: PublicLandingContentComponent;
  let fixture: ComponentFixture<PublicLandingContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicLandingContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicLandingContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
