import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-public-landing-content',
  templateUrl: './public-landing-content.component.html',
  styleUrls: ['./public-landing-content.component.css']
})
export class PublicLandingContentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.log('PublicLandingContentComponent');
  }

}
