import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { PublicWebsiteRoutingModule } from './public-website-routing.module';
import { PublicLandingComponent } from './public-landing/public-landing.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgMaterialModule } from 'src/app/shared/modules/ng-material/ng-material.module';


@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [PublicLandingComponent],
  imports: [
    CommonModule,   
    PublicWebsiteRoutingModule,    
   SharedModule.forRoot()
  ],
  exports: [
  ]
})
export class PublicWebsiteModule { }
