import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteHeaderComponent } from './site-header.component';
import { UserManageService } from 'src/app/shared/services/api/user-service/user-manage.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { NgBootstrapModule } from 'src/app/shared/modules/ng-bootstrap/ng-bootstrap.module';
import { NgMaterialModule } from 'src/app/shared/modules/ng-material/ng-material.module';
import { Router } from '@angular/router';

describe('SiteHeaderComponent', () => {
  let component: SiteHeaderComponent;
  let fixture: ComponentFixture<SiteHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteHeaderComponent ],
      imports: [ NgBootstrapModule , NgMaterialModule],
      providers:[UserManageService, AuthService, {provide: Router}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
