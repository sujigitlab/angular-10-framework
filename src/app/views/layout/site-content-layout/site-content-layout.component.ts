import { Component, OnInit, Inject } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-site-content-layout',
  templateUrl: './site-content-layout.component.html',
  styleUrls: ['./site-content-layout.component.css']
})
export class SiteContentLayoutComponent implements OnInit {
  isLanding = false;
  isAdminPanel = false;
  constructor(@Inject(Router) private router) {
    router.events.subscribe((event: RouterEvent) => {
      if ( event instanceof NavigationEnd) {
        // console.log(this.router.url);
        this.isLanding = this.router.url.indexOf('landing') > 0;
        this.isAdminPanel = this.router.url.indexOf('admin') > 0;
      }
    });
   }
  ngOnInit() {
  }

}
