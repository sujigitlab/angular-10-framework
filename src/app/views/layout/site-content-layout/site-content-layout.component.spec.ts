import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteContentLayoutComponent } from './site-content-layout.component';
import { SiteContentComponent } from './site-content/site-content.component';
// import { Router } from '@angular/router';

describe('SiteContentLayoutComponent', () => {
  let component: SiteContentLayoutComponent;
  let fixture: ComponentFixture<SiteContentLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteContentLayoutComponent, SiteContentComponent ],
      providers: []
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteContentLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
