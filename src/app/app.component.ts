import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import { environment } from '../environments/environment';
import { SharedMessageService } from 'src/app/shared/data-bus/shared-message-services/shared-message.service';
import { Router } from '@angular/router';
import { HttpStatusService } from './core/services/http-status/http-status.service';
import { NavigationInterceptorService } from './core/interceptors/navigation/navigation-interceptor.service';
import * as AOS from 'aos';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  httpStatusSubscriber: any;
  title = 'tat-suite';
  env = environment;
  HTTPActivity = false;
  httStatusEventTimer = null;
  constructor(@Inject(UserIdleService) private userIdle,
              @Inject(SharedMessageService) private sharedMessageService,
              @Inject(Router) private router,
              @Inject(HttpStatusService) private httpStatus,
              @Inject(NavigationInterceptorService) private navigationInterceptor
              ) {
              this.subscribeHttpStatus();
              this.navigationInterceptor.registerNavigationInterceptorEvent(this.router);
  }
  ngOnInit(): void {
    console.log('AppComponent  AuthService');
    AOS.init();
  }
  ngOnDestroy(): void {
    localStorage.clear();
    this.unsubscribeHttpStatus();
  }
  subscribeHttpStatus(): void {
    this.httpStatusSubscriber = this.httpStatus.getHttpStatus().subscribe(
      (status: boolean) => {
          // console.log(status);
          this.clearHttStatusEventTimer();
          this.httStatusEventTimer = setTimeout(() => {
            this.HTTPActivity = status;
           // console.log('HTTPActivity while routing -->', this.HTTPActivity);
          });
        }
    );
  }
  unsubscribeHttpStatus(): void {
    if (this.httpStatusSubscriber) {
        this.httpStatusSubscriber.unsubscribe();
    }
  }
  clearHttStatusEventTimer(): void {
    if (this.httStatusEventTimer) {
      clearTimeout(this.httStatusEventTimer);
    }
  }
}
