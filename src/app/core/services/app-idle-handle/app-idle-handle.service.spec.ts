import { TestBed } from '@angular/core/testing';

import { AppIdleHandleService } from './app-idle-handle.service';
import { UserManageService } from 'src/app/shared/services/api/user-service/user-manage.service';
import { UserIdleService } from 'angular-user-idle';
import { AuthService } from '../auth/auth.service';

// describe('AppIdleHandleService', () => {
//   beforeEach(() => TestBed.configureTestingModule({
//     providers: [UserManageService, UserIdleService, AuthService]
//   }));

//   it('should be created', () => {
//     const service: AppIdleHandleService = TestBed.get(AppIdleHandleService);
//     expect(service).toBeTruthy();
//   });
// });
