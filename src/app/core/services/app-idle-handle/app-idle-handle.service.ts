import { Injectable, Inject } from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import { AuthService } from '../auth/auth.service';
import { UserManageService } from 'src/app/shared/services/api/user-service/user-manage.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { UserConfirmDialogComponent } from 'src/app/shared/modal/user-confirm-dialog/user-confirm-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class AppIdleHandleService {
  refreshSubscriber: any = null;
  isValidSession = false;
  userIdle: any;
  confrm: boolean;
  constructor(@Inject(AuthService) private authService,
              @Inject(UserManageService) private userManageService,
              private router: Router,
              public dialog: MatDialog) { }
  confirm = false;
  count = 0;
  clearTimeOut = null;
  initIdleActivities(userIdle: UserIdleService): void {
    console.log('Entered into Init initIdleActivities');
    this.userIdle = userIdle;
    // Start watching for user inactivity.
    this.userIdle.startWatching();
    console.log('Started watching for Idle');
    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {
      this.count = count;
      console.log('Count ', this.count);
      if (this.isValidSession && this.count === 286 ) {// 286
        console.log('comming');
        const eventList = ['click', 'mouseover', 'keydown', 'DOMMouseScroll', 'mousewheel',
        'mousedown', 'touchstart', 'touchmove', 'scroll', 'keyup'];
        for (const event of eventList) {
        document.body.addEventListener(event, () => {
          return this.restart();
        } );
        }
        const dialog: any = this.dialog.open(UserConfirmDialogComponent, {
          width: '30vw',
          disableClose: true,
         // closeOnNavigation: true,
          panelClass: 'confirm-dialog-container',
          data: {
            message: 'Your session will be expired! Click on OK to resume the application.'
          }
        });
        dialog.afterOpened().subscribe(res => {
         // console.log('######opened#########', res);
         // console.log(dialog);
          this.clearTimeOut = setTimeout(() => {
            try {
              const isYes =  dialog.componentInstance.isYes;
              this.confrm = isYes;
              console.log('this.confrm', this.confrm);
            } catch (e) {

            }
            if (!this.confrm) {
              dialog.close();
            }
          }, 10000);
        });
        dialog.afterClosed().subscribe(res => {
         // console.log('######closed#########');
          if (this.clearTimeOut) {
            clearTimeout(this.clearTimeOut);
          }
          try {
            const isYes =  dialog.componentInstance.isYes;
            this.confirm = isYes;
            // console.log('this.confrm', this.confrm);
            } catch (e) {

            }
          if (this.confirm) {
             // this.stop();
              // this.stopWatching();
              this.restart();
             // this.startWatching();
              this.refreshToken();
            } else {
              this.authService.reset();
              this.router.navigate(['/']);
              this.stop();
              this.stopWatching();
            }
          this.dialog.closeAll();
        });

      }
    });
    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {
      console.log('Time is up!', this.count, this.confirm );

    });
    this.refreshSubscriber = this.authService.getIsValidSession()
    .subscribe((isSessionStarted) => {
     console.log('Session Started:', isSessionStarted);
     this.isValidSession = isSessionStarted;
    });
    this.userIdle.ping$.subscribe(() => {
      console.log('PING --. this.isValidSession', this.isValidSession);
      if (this.isValidSession) {
          this.refreshToken();
        }
  });
  }
  refreshToken(): void {
    console.log('Entered into refreshToken ', this.isValidSession);
    if (this.isValidSession) {
      console.log('Entered into refreshToken ');
      this.userManageService.refreshToken().subscribe((response) => {
            console.log(response);
            this.authService.initAuthData(response);
      }, (error) => {
        console.log(error);
      });
  }
  }
  stop(): void {
    console.log('stop timer');
    this.userIdle.stopTimer();
  }
  stopWatching(): void {
    console.log('stop Watching');
    this.userIdle.stopWatching();
  }
  startWatching(): void {
    console.log('Start Watching');
    this.userIdle.startWatching();
  }
  restart(): void {
   // console.log('reset timer');
    this.userIdle.resetTimer();
    this.userIdle.startWatching();
  }
}
