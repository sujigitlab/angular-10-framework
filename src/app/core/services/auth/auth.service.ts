import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isValidSession: BehaviorSubject<boolean>;
  private isServiceAdvisor: BehaviorSubject<boolean>;
  private isSysAdmin: BehaviorSubject<boolean>;
  private isSuperAdmin: BehaviorSubject<boolean>;
  private isSupervisor: BehaviorSubject<boolean>;
  private isWorkshopAdmin: BehaviorSubject<boolean>;
  private isBodyshopDept: BehaviorSubject<boolean>;
  private isMechanicalDept: BehaviorSubject<boolean>;

  private isElectricalDept: BehaviorSubject<boolean>;
  private isPaintshopDept: BehaviorSubject<boolean>;
  private isPolishshopDept: BehaviorSubject<boolean>;
  private isBodykitDept: BehaviorSubject<boolean>;
  private userFullName: BehaviorSubject<string>;
  private userName: BehaviorSubject<string>;
  private token: BehaviorSubject<string>;
  private role: BehaviorSubject<string>;
  private userType: BehaviorSubject<string>;
  private businessCustomerNumber: BehaviorSubject<string>;
  constructor() {
    this.isValidSession = new BehaviorSubject(false);
    this.isServiceAdvisor = new BehaviorSubject(false);
    this.isSysAdmin = new BehaviorSubject(false);
    this.isSuperAdmin = new BehaviorSubject(false);
    this.isSupervisor = new BehaviorSubject(false);
    this.isWorkshopAdmin = new BehaviorSubject(false);
    this.isBodyshopDept = new BehaviorSubject(false);
    this.isMechanicalDept = new BehaviorSubject(false);
    this.isElectricalDept = new BehaviorSubject(false);
    this.isPaintshopDept = new BehaviorSubject(false);
    this.isPolishshopDept = new BehaviorSubject(false);
    this.isBodykitDept = new BehaviorSubject(false);
    this.userFullName = new BehaviorSubject(null);
    this.userName = new BehaviorSubject(null);
    this.token = new BehaviorSubject(null);
    this.role = new BehaviorSubject(null);
    this.userType = new BehaviorSubject(null);
    this.businessCustomerNumber = new BehaviorSubject(null);
  }

  initAuthData(authData: any, userFullName?: any): void {

    console.log('Entered into Init AuthData');
    if (null != authData) {
      localStorage.setItem('autData', JSON.stringify(authData));
    } else {
      const authDataStr = localStorage.getItem('autData');
      if (authDataStr) {
        authData =  JSON.parse(authDataStr);
        if (userFullName) {
          authData.fullName = userFullName;
          localStorage.setItem('autData', JSON.stringify(authData));
        }
      }
    }
    console.log(authData);
    if (authData) {
      this.setIsValidSession(true);
      this.setUserFullName(authData.fullName);
      this.setUserName(authData.username);
      this.setRole(authData.roles);
      this.setBusinessCustomerNumber(authData.businessCustomerNumber);
      const roles = authData.roles;
      if (roles.indexOf('ROLE_SERVICEADVISOR') > -1) {
        this.setIsServiceAdvisor(true);
      } else if (roles.indexOf('ROLE_SYSADMIN') > -1) {
        this.setIsSysAdmin(true);
       } else if (roles.indexOf('ROLE_SUPERADMIN') > -1) {
        this.setIsSuperAdmin(true);
      } else if (roles.indexOf('ROLE_SUPERVISOR') > -1) {
        this.setIsSupervisor(true);
      } else if (roles.indexOf('ROLE_WORKSHOPADMIN') > -1) {
        this.setIsWorkshopAdmin(true);
      } else if (roles.indexOf('ROLE_BODYSHOP') > -1) {
        this.setIsBodyshopDept(true);
      } else if (roles.indexOf('ROLE_MECHANICALWORKSHOP') > -1) {
        this.setIsMechanicalDept(true);

      } else if (roles.indexOf('ROLE_ELECTRICALWORKSHOP') > -1) {
        this.setIsElectricalDept(true);
      } else if (roles.indexOf('ROLE_PAINTSHOP') > -1) {
        this.setIsPaintshopDept(true);
      } else if (roles.indexOf('ROLE_POLISH') > -1) {
        this.setIsPolishshopDept(true);
      } else if (roles.indexOf('ROLE_BODYKIT') > -1) {
        this.setIsBodykitDept(true);
       }
      this.setToken(authData.token);
    }
  }
  public setIsValidSession(sessionFlag: boolean): void {
    console.log('reset session', sessionFlag);
    this.isValidSession.next(sessionFlag);
  }
  public getIsValidSession(): Observable<boolean> {
    return this.isValidSession.asObservable();
  }
  public getIsServiceAdvisor(): Observable<boolean> {
    return this.isServiceAdvisor.asObservable();
  }
  public getIsSysAdmin(): Observable<boolean> {
    return this.isSysAdmin.asObservable();
  }
  public getIsSuperAdmin(): Observable<boolean> {
    return this.isSuperAdmin.asObservable();
  }
  public getIsSupervisor(): Observable<boolean> {
    return this.isSupervisor.asObservable();
  }
  public getIsWorkshopAdmin(): Observable<boolean> {
    return this.isWorkshopAdmin.asObservable();
  }
  public getIsBodyshopDept(): Observable<boolean> {
    return this.isBodyshopDept.asObservable();
  }
  public getIsMechanicalDept(): Observable<boolean> {
    return this.isMechanicalDept.asObservable();
  }
  public getIsElectricalDept(): Observable<boolean> {
    return this.isElectricalDept.asObservable();
  }
  public getIsPaintshopDept(): Observable<boolean> {
    return this.isPaintshopDept.asObservable();
  }
  public getIsPolishshopDept(): Observable<boolean> {
    return this.isPolishshopDept.asObservable();
  }
  public getIsBodykitDept(): Observable<boolean> {
    return this.isBodykitDept.asObservable();
  }
  public setIsServiceAdvisor(adminFlag: boolean): void {

    this.isServiceAdvisor.next(adminFlag);
  }
  public setIsSysAdmin(adminFlag: boolean): void {

    this.isSysAdmin.next(adminFlag);
  }
  public setIsSuperAdmin(adminFlag: boolean): void {

    this.isSuperAdmin.next(adminFlag);
  }
  public setIsWorkshopAdmin(adminFlag: boolean): void {

    this.isWorkshopAdmin.next(adminFlag);
  }
  public setIsBodyshopDept(adminFlag: boolean): void {

    this.isBodyshopDept.next(adminFlag);
  }

  public setIsElectricalDept(adminFlag: boolean): void {

    this.isElectricalDept.next(adminFlag);
  }
  public setIsPaintshopDept(adminFlag: boolean): void {

    this.isPaintshopDept.next(adminFlag);
  }
  public setIsPolishshopDept(adminFlag: boolean): void {

    this.isPolishshopDept.next(adminFlag);
  }
  public setIsBodykitDept(adminFlag: boolean): void {

    this.isBodykitDept.next(adminFlag);
  }

  public setIsMechanicalDept(adminFlag: boolean): void {

    this.isMechanicalDept.next(adminFlag);
  }
  public setIsSupervisor(adminFlag: boolean): void {

    this.isSupervisor.next(adminFlag);
  }
  public getUserFullName(): Observable<string> {
    return this.userFullName.asObservable();
  }
  public getUserName(): Observable<string> {
    return this.userName.asObservable();
  }
  public getRoles(): Observable<string> {
    return this.role.asObservable();
  }
  public getUserType(): Observable<string> {
    return this.userType.asObservable();
  }
  public getBusinessCustomerNumber(): Observable<string> {
    return this.businessCustomerNumber.asObservable();
  }
  public setUserFullName(fullName: string): void {
    this.userFullName.next(fullName);
  }
  public setUserName(name: string): void {
    this.userName.next(name);
  }
  public setRole(name: string): void {
    this.role.next(name);
  }
  public setUserType(name: string): void {
    this.userType.next(name);
  }
  public getToken(): Observable<string> {
    return this.token.asObservable();
  }
  public setToken(token: string): void {
    this.token.next(token);
  }
  public setBusinessCustomerNumber(num: string): void {
    this.businessCustomerNumber.next(num);
  }
  public reset(): void {
    console.log('reset session');
    this.setIsValidSession(false);
    this.setUserFullName(null);
    this.setIsServiceAdvisor(false);
    this.setIsSysAdmin(false);
    this.setIsSuperAdmin(false);
    this.setIsSupervisor(false);
    this.setIsWorkshopAdmin(false);
    this.setIsBodyshopDept(false);
    this.setIsElectricalDept(false);
    this.setIsPaintshopDept(false);
    this.setIsPolishshopDept(false);
    this.setIsBodykitDept(false);



    this.setToken(null);
    this.setBusinessCustomerNumber(null);
    localStorage.clear();
  }

}
