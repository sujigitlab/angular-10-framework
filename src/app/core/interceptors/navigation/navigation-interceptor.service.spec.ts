import { TestBed } from '@angular/core/testing';

import { NavigationInterceptorService } from './navigation-interceptor.service';
import { Router } from '@angular/router';
import { HttpStatusService } from '../../services/http-status/http-status.service';

describe('NavigationInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{provide: Router}, {provide: HttpStatusService}]
  }));

  // it('should be created', () => {
  //   const service: NavigationInterceptorService = TestBed.get(NavigationInterceptorService);
  //   expect(service).toBeTruthy();
  // });
});
