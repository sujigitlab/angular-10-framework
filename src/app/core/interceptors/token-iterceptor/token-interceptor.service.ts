import { Injectable, Inject } from '@angular/core';

import { HttpRequest, HttpInterceptor, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {
  authToken: string = null;
  constructor(  @Inject(AuthService) private authService) {
    this.subscribeToken();
  }
  subscribeToken(): void {
     this.authService.getToken().subscribe(
       (authToken) => {
         this.authToken = authToken;
       }
      );
   }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // console.log('localstoragetoken');
  //  console.log(this.authToken);
    if ( null != this.authToken) {
    req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${this.authToken}`,
        ClientTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
      },
      // withCredentials: true
    });
    }
    // console.log(req);
    return next.handle(req);
  }
}
