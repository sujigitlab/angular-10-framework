import { TestBed } from '@angular/core/testing';

import { HttpListenerService } from './http-listener.service';
import { HttpStatusService } from '../../services/http-status/http-status.service';
import { Router } from '@angular/router';

describe('HttpListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [HttpStatusService, {provide: Router} ]
  }));

  it('should be created', () => {
    const service: HttpListenerService = TestBed.get(HttpListenerService);
    expect(service).toBeTruthy();
  });
});
