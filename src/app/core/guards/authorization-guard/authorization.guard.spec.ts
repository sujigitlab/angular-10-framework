import { TestBed, async, inject } from '@angular/core/testing';

import { AuthorizationGuard } from './authorization.guard';
import { AuthService } from '../../services/auth/auth.service';

describe('AuthorizationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthorizationGuard, AuthService]
    });
  });

  // it('should ...', inject([AuthorizationGuard], (guard: AuthorizationGuard) => {
  //   expect(guard).toBeTruthy();
  // }));
});
