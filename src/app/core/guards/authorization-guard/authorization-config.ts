export const AUTHORIZATION_CONFIG = {
    '/landing' : 'AUTHORIZED',
    '/denied' : 'AUTHORIZED',
    '/404' : 'AUTHORIZED',
    '/env-check' : 'AUTHORIZED',
    '/home' : 'AUTHORIZED',
    '/public': 'AUTHORIZED',
    '/public/landing': 'AUTHORIZED',
    '/public/landing/pw-site-content': 'AUTHORIZED',
    '/changepassword': 'AUTHORIZED',
    '/login': 'AUTHORIZED',
    '/login/landing': 'AUTHORIZED',
    '/login/changepassword': 'AUTHORIZED'
    };
