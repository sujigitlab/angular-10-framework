import { RestClientProvider } from './rest-client-provider';
import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

describe('RestClientProvider', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{provide: HttpClient}]
  }));
  // it('should create an instance', () => {
  //   expect(new RestClientProvider()).toBeTruthy();
  // });
});
