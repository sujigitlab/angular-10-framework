import { NgModule, Optional, SkipSelf } from '@angular/core';
import { NavigationInterceptorService } from './interceptors/navigation/navigation-interceptor.service';
import { HttpStatusService } from './services/http-status/http-status.service';
import { HttpListenerService } from './interceptors/http-listener/http-listener.service';
import { AppIdleHandleService } from './services/app-idle-handle/app-idle-handle.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthorizationGuard } from './guards/authorization-guard/authorization.guard';
import { AlertToasterNotificationsModule } from 'src/app/core/alert-toaster-notifications';
import { TokenInterceptorService } from './interceptors/token-iterceptor/token-interceptor.service';
import { AuthService } from './services/auth/auth.service';
import { CommonModule } from '@angular/common';
const CORE_SERVICES = [NavigationInterceptorService, HttpStatusService,
   AppIdleHandleService, AuthService];
const CORE_GUARDS = [AuthorizationGuard];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    AlertToasterNotificationsModule
  ],
  providers: [/** SERVICES ARE HERE */
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpListenerService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    ...CORE_SERVICES,
        ...CORE_GUARDS,
  ],
  exports: [ AlertToasterNotificationsModule ]
})
export class CoreModule {
   constructor(@Optional() @SkipSelf() core: CoreModule) {
      if (core) {
        throw new Error('You shall nor Run !!!');
      }
   }
 }
