import { ExtraOptions } from '@angular/router';
// https://www.bennadel.com/blog/3545-enabling-the-second-click-of-a-routerlink-fragment-using-onsameurlnavigation-reload-in-angular-7-1-3.htm
export const config: ExtraOptions = {
    useHash: true,
    scrollPositionRestoration: 'enabled',
    onSameUrlNavigation: 'reload',
    enableTracing: false // <-- debugging purposes only
  };
