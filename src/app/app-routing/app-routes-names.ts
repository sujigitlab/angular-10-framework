export const APP_ROUTE_NAMES = {
    NOT_FOUND_PATH: '404',
    NOT_MATCHED_PATH: '**',
    EMPTY_PATH: '',
    DENIED_PATH: 'denied',
    HOME_PATH: 'home',
    LANDING_PATH: 'landing',
    ENV_CHECK_PATH: 'env-check',
    SERVICE_LANDING_PATH: 'service/landing',
    VEHICLE_PATH: 'vehicle',
    CUSTOMER_DASHBOARD: 'customer_dashboard',

  };
