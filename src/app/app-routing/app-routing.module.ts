import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { APP_ROUTES } from 'src/app/app-routing/app-routes';
import { config } from 'src/app/app-routing/app-routing-config';


// https://www.tektutorialshub.com/angular/angular-preloading-strategy/
// https://codinglatte.com/posts/angular/lazy-loading-modules-preloading-strategy-in-angular-8/
// tslint:disable-next-line:max-line-length
// https://www.freakyjolly.com/angular-nested-routing-with-multiple-routeroutlet-using-loadchildren-having-own-router-modules-example-application/

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(APP_ROUTES, config)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
