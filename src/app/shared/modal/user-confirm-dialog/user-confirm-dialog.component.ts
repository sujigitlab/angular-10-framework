import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-user-confirm-dialog',
  templateUrl: './user-confirm-dialog.component.html',
  styleUrls: ['./user-confirm-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserConfirmDialogComponent implements OnInit {
  public isYes = false;

  constructor( public dialogRef: MatDialogRef<UserConfirmDialogComponent>,
               @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit(): void {
  }
  yes(): void {
    this.isYes = true;
    this.dialogRef.close();
}

close(): void {
    this.dialogRef.close();
}
}
