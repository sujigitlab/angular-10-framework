import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { SUBJECT_NAMES } from './shared-message-subject-names';

@Injectable()
export class SharedMessageService {
  constructor() {
  this.createSubject();
  }
 static subjectMap = new Map<string, Subject<any>>();
  static displaySubjectMapInfo(): void {
    console.log('SharedMessageService Subjects Length :', SharedMessageService.subjectMap.size);
    for (const keys in SharedMessageService.subjectMap) {
      if (SharedMessageService.subjectMap.hasOwnProperty(keys)) {
        console.log(keys);
    }
     }
  }

  private createSubject(): void {

    for (const subjectName of SUBJECT_NAMES) {
      // console.log(subjectName);
      SharedMessageService.subjectMap.set(subjectName, new Subject<any>());
    }
  }
  sendMessage(message: any, subjectIdentifier: string): void {
    // console.log(subjectIdentifier, '############################', message);
    SharedMessageService.subjectMap.get(subjectIdentifier).next({ data: message });

  }
  clearMessages(subjectIdentifier: string): void {
    try {
      SharedMessageService.subjectMap.get(subjectIdentifier).next();
    } catch (e) {

    }
}

  getMessage(subjectIdentifier: string): Observable<any> {
    return SharedMessageService.subjectMap.get(subjectIdentifier).asObservable();
  }
}
