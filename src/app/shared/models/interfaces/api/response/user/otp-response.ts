import { OtpPayload } from './otp-payload';

export interface OtpResponse extends OtpPayload {
  status: string;
  message: string;
  payload: OtpPayload;
}
