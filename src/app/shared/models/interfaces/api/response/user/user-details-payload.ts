

export interface UserDetailsPayload {

    id: number;
    userType: string;
    gender: string;
    dob: string;
    idProofNumber: string;
    idProofType: string;
    postal: string;
    addressLine1: string;
      addressLine2: string;
      altMobileCode: string;
      altMobileNumber: string;
      landLineCode: string;
      landLineNumber: string;
     taxId: string;
     addTaxId: string;
     email: string;
     userName: string;
     username: string;
     createdBy: string;
    status: string;
    enabled: boolean;
    taxExemptionCertificate: string;
    profession: string;
    remark: string;
    firstName: string;
    firstname: string;
    userCode: string;
    usercode: string;
    addressNumber: string;
    middleName: string;
    middlename: string;
    lastName: string;
    lastname: string;
    fullFaxNumber: string;
    faxAreaCode: string;
    faxLocalNumber: string;
    flag: string;
    addressnumber: string;
  }


