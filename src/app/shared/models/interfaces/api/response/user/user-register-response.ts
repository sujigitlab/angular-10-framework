import { UserRegisterUser } from './user-register-user';

export interface UserRegisterResponse {
  status: string;
  user: UserRegisterUser;


}
