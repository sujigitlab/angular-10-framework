import { UserDetailsPayload } from './user-details-payload';

export interface UserDetailsResponse extends UserDetailsPayload {

  status: string;
  message: string;
  payload: UserDetailsPayload[];
  totalCount: number;
}
