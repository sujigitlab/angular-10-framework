export interface OtpPayload {
  otp: number;
  mobileNumber: number;
}
