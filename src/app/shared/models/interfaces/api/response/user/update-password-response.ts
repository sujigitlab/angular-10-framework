import { UpdatePasswordPayload } from './update-password-payload';

export interface UpdatePasswordResponse extends UpdatePasswordPayload {
  status: string;
  user: UpdatePasswordPayload;
}
