export interface UserRegisterUser {
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  mobileNumber: number;
  // enabled: boolean;
}

