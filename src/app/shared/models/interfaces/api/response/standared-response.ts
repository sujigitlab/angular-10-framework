export interface StandaredResponse {
  status: string;
  message: string;
  messageCode: number;
  payload: any;
}
