export class PageParams {
  offSet = 0;
  limit = 1000;
  sortField = '';
  sortOrder = 'DESC';

  constructor(
    offSet: number, limit: number, sortField: string, sortOrder: string) {
      this.offSet = offSet;
      this.limit = limit;
      this.sortField = sortField;
      this.sortOrder = sortOrder;
    }
}
