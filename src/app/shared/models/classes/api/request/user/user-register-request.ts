export class UserRegisterRequest {
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  mobileNumber: number;
  // enabled = true;
  constructor(
    username: string,
    password: string,
    firstname: string,
    lastname: string,
    mobileNumber: number,
    // enabled: boolean
    ) {
    this.username = username;
    this.password = password;
    this.firstname = firstname;
    this.lastname = lastname;
    this.mobileNumber = mobileNumber;
    // this.enabled = enabled;
  }
}
