import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { NgMaterialModule } from 'src/app/shared/modules/ng-material/ng-material.module';
import { RouterModule } from '@angular/router';
import { SharedEventEmitterBusService } from 'src/app/shared/data-bus/shared-event-emitter-bus.service';
import { SharedMessageService } from 'src/app/shared/data-bus/shared-message-services/shared-message.service';
import { NgBootstrapModule } from 'src/app/shared/modules/ng-bootstrap/ng-bootstrap.module';
import { MiscellaneousModule } from 'src/app/shared/modules/miscellaneous/miscellaneous.module';
import { UserManageService } from './services/api/user-service/user-manage.service';
import { PrintErrorComponent } from './components/print-error/print-error.component';


@NgModule({
  entryComponents: [
    ],
  imports: [
    CommonModule,
     NgMaterialModule,
    NgBootstrapModule, MiscellaneousModule
 ],
  // tslint:disable-next-line: max-line-length
  declarations: [ PrintErrorComponent /* PIPES, COMPONENTS, DIRECTIVES */
         ],

  exports: [
    CommonModule,
    // tslint:disable-next-line:max-line-length
    NgMaterialModule, NgBootstrapModule, MiscellaneousModule, PrintErrorComponent/* PIPES, COMPONENTS, DIRECTIVES, MODULES */],
     /** NO PROVIDERS HERE */
})
export class SharedModule {

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: SharedModule,
      providers: [
        /** PROVIDERS ARE HERE */
        SharedEventEmitterBusService,
        SharedMessageService,
        UserManageService,
        DatePipe
        // MessagingService
        ]
    };
  }
 }
