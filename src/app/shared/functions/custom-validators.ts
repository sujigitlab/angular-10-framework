import { AbstractControl } from '@angular/forms';

export function validateMobile(control: AbstractControl): any {
  if (isNaN(control.value)) {
    return { mobile: true };
  }
  return null;
}
export function validateMobileMinLength(control: AbstractControl): any  {
    if (control.value && control.value.length < 9) {
      return { mobileMinLength: true };
    }
    return null;
  }
export function validateMobileMaxLength(control: AbstractControl): any  {
    if (control.value && control.value.length > 10) {
      return { mobileMaxLength: true };
    }
    return null;
}
export function validateAlbhabets(control: AbstractControl): any  {
    const name =  control.value;
    const pattern = /^[a-zA-Z@&_.#/ ]*$/;
    // /^[a-zA-Z0-9@&$ ]*$/;
    // ^[a-zA-Z ]*$

    if (name && name.match(pattern) == null) {
      return { validateAlphabets: true };
    }
    return null;
}
export function validateMinLength3(control: AbstractControl): any  {
    const name =  control.value;
    if (name && name.length < 3) {
        return { minlength3: true };
      }
    return null;
}
export function validateMinLength8(control: AbstractControl): any  {
  const name =  control.value;
  if (name && name.length < 8) {
      return { minlength8: true };
    }
  return null;
}
export function validatePassword(control: AbstractControl): any  {
    const p = control.value;
    if (p.length < 8) {
        return { minlength8: true };
    }
    if (p.search(/[a-zA-Z]/i) < 0) {
        return { atleastOneCharacter: true };
    }
    if (p.search(/[0-9]/) < 0) {
        return { atleastOneDigit: true };
    }
    return null;
}
