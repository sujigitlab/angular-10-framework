import { Injectable, Inject } from '@angular/core';
import { RestClientProvider } from 'src/app/core/rest-client-provider/rest-client-provider';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UserLoginRequest } from 'src/app/shared/models/classes/api/request/user/user-login-request';
import { UserLoginResponse } from 'src/app/shared/models/interfaces/api/response/user-login-response';
import { Observable, throwError } from 'rxjs';
import { UserRegisterRequest } from 'src/app/shared/models/classes/api/request/user/user-register-request';
import { UserRegisterResponse } from 'src/app/shared/models/interfaces/api/response/user/user-register-response';
import { OtpResponse } from 'src/app/shared/models/interfaces/api/response/user/otp-response';
import { UserDetailsResponse } from 'src/app/shared/models/interfaces/api/response/user/user-details-response';
import { UpdatePasswordRequest } from 'src/app/shared/models/classes/api/request/user/update-password-request';
import { UpdatePasswordResponse } from 'src/app/shared/models/interfaces/api/response/user/update-password-response';


@Injectable({
  providedIn: 'root'
})
export class UserManageService extends RestClientProvider {

  constructor(@Inject(HttpClient) public httpClient) {
    super(httpClient, 'api-1');
    // console.log(this.apiEndpoints);
  }
  craeteUser(userRegisterRequest: UserRegisterRequest, roleList?: any): Observable<UserRegisterResponse> {
    let params = new HttpHeaders();
    if (!roleList) {
      roleList = ['ROLE_USER'];
    }
    params = params.append('roles', roleList);
    return this.post(`${this.apiEndpoints.userRegister}`, userRegisterRequest, { headers: params }) as Observable<UserRegisterResponse>;
  }
  userLogin(loginRequest: UserLoginRequest): Observable<UserLoginResponse> { // getAll
    let params = new HttpHeaders();
    params = params.append('isSkipPageLoad', 'true');
    //   // tslint:disable-next-line:no-angle-bracket-type-assertion
    return this.post(`${this.apiEndpoints.userLogin}`, loginRequest, { headers: params }) as Observable<UserLoginResponse>;
  }
  login(loginRequest: UserLoginRequest): Observable<UserLoginResponse> { // getAll
    //   // tslint:disable-next-line:no-angle-bracket-type-assertion
    return this.post(`${this.apiEndpoints.login}`, loginRequest)  as Observable<UserLoginResponse>;
  }
  logout(): Observable<any> { // getAll
    //   // tslint:disable-next-line:no-angle-bracket-type-assertion
    return this.get(`${this.apiEndpoints.logout}`) as Observable<any>;
  }
  refreshToken(): Observable<UserLoginResponse> { // getAll
    let params = new HttpHeaders();
    params = params.append('isSkipPageLoad', 'true');
    //   // tslint:disable-next-line:no-angle-bracket-type-assertion
    return this.get(`${this.apiEndpoints.refreshToken}`, { headers: params }) as Observable<UserLoginResponse>;
  }
  createOtp(countryCode: string, mobileNumber: string): Observable<OtpResponse> {
    // tslint:disable-next-line:max-line-length
    return this.get(`${this.apiEndpoints.otp}/${countryCode}/${mobileNumber}`) as Observable<OtpResponse>;
  }
  verifyOtp(countryCode: string, mobileNumber: string, otp: number): Observable<OtpResponse> {
    // tslint:disable-next-line:max-line-length
    return this.get(`${this.apiEndpoints.otp}/${countryCode}/${mobileNumber}/${otp}`) as Observable<OtpResponse>;
  }
 generateotp(countryCode: string, mobileNumber: string): Observable<OtpResponse> {
  // tslint:disable-next-line:max-line-length
  return this.get(`${this.apiEndpoints.enqueryotp}/${countryCode}/${mobileNumber}`) as Observable<OtpResponse>;
  }


}

