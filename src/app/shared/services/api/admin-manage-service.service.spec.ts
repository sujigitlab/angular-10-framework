import { TestBed } from '@angular/core/testing';

import { AdminManageService } from './admin-manage-service.service';

describe('AdminManageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminManageService = TestBed.get(AdminManageService);
    expect(service).toBeTruthy();
  });
});
