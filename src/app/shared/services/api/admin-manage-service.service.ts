import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { RestClientProvider } from 'src/app/core/rest-client-provider/rest-client-provider';
import { Observable } from 'rxjs';
import { UserRegisterResponse } from '../../models/interfaces/api/response/user/user-register-response';

@Injectable({
  providedIn: 'root'
})
export class AdminManageService extends RestClientProvider {

  constructor(@Inject(HttpClient) public httpClient) {
    super(httpClient, 'api-1');
    // console.log(this.apiEndpoints);
  }

 
//   createUser(registrationRequest: RegistrationRequest, userType: string): Observable<RegistrationResponse> {
//     let params = new HttpHeaders();
//     params = params.append('usertype', userType);
//     return this.post(`${this.apiEndpoints.createAdminUser}`, registrationRequest, { headers: params }) as Observable<RegistrationResponse>;
//   }
//   updateUser(registrationRequest: RegistrationRequest, userType: string): Observable<RegistrationResponse> {
//     let params = new HttpHeaders();
//     params = params.append('usertype', userType);
//     return this.put(`${this.apiEndpoints.updateAdminUser}`, registrationRequest, { headers: params }) as Observable<RegistrationResponse>;
//   }
//   activateUser(id: number): Observable<ActivateInactivateResponse> { // getAll
//     //   // tslint:disable-next-line:no-angle-bracket-type-assertion
//     return this.put(`${this.apiEndpoints.activateUser}/${id}`, id) as Observable<ActivateInactivateResponse>;
//   }
//   deactivateUser(id: number): Observable<ActivateInactivateResponse> { // getAll
//     //   // tslint:disable-next-line:no-angle-bracket-type-assertion
//     return this.delete(`${this.apiEndpoints.deactivateUser}/${id}`, id) as Observable<ActivateInactivateResponse>;
//   }

//   createOffer(createOffersRequest: CreateOffersRequest, fileToUpload: File): Observable<FetchOffersResponse> { // uploadFile
//     const formdata: FormData = new FormData();
//     // console.log( fileToUpload);
//     formdata.append('imageFile', fileToUpload);
//     // console.log(formdata.get('imageFile'));
//     formdata.append('offerRequest', JSON.stringify(createOffersRequest));
//     // console.log(formdata.get('galleryImageRequest'));
//     return this.post(`${this.apiEndpoints.addOrEditOffer}`, formdata) as Observable<FetchOffersResponse>;
//   }
//   updateOffer(createOffersRequest: CreateOffersRequest, fileToUpload: File): Observable<FetchOffersResponse> { // uploadFile
//     const formdata: FormData = new FormData();
//     // console.log( fileToUpload);
//     formdata.append('imageFile', fileToUpload);
//     // console.log(formdata.get('imageFile'));
//     formdata.append('offerRequest', JSON.stringify(createOffersRequest));
//     // console.log(formdata.get('galleryImageRequest'));
//     return this.put(`${this.apiEndpoints.addOrEditOffer}`, formdata) as Observable<FetchOffersResponse>;
//   }
//   fetchOffers(fetchOffersRequest: FetchOffersRequest): Observable<FetchOffersResponse> { // getAll
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.fetchOffers}`, fetchOffersRequest) as Observable<FetchOffersResponse>;
//   }
//   activateOffer(id: number): Observable<FetchOffersResponse> { // getAll
//     //   // tslint:disable-next-line:no-angle-bracket-type-assertion
//     return this.put(`${this.apiEndpoints.addOrEditOffer}/${id}`, id) as Observable<FetchOffersResponse>;
//   }
//   inActivateOffer(id: number): Observable<FetchOffersResponse> { // getAll
//     //   // tslint:disable-next-line:no-angle-bracket-type-assertion
//     return this.delete(`${this.apiEndpoints.addOrEditOffer}/${id}`, id) as Observable<FetchOffersResponse>;
//   }
//   fetchvalidOffers(fetchOffersRequest: FetchOffersRequest): Observable<FetchOffersResponse> { // getvalid offers
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.validoffers}`, fetchOffersRequest) as Observable<FetchOffersResponse>;
//   }
//   createNews(createNewsRequest: CreateNewsRequest, fileToUpload: File): Observable<FetchNewsResponse> { // uploadFile
//     const formdata: FormData = new FormData();
//     // console.log( fileToUpload);
//     formdata.append('Image File', fileToUpload);
//     // console.log(formdata.get('imageFile'));
//     formdata.append('News Request', JSON.stringify(createNewsRequest));
//     // console.log(formdata.get('galleryImageRequest'));
//     return this.post(`${this.apiEndpoints.addOrEditNews}`, formdata) as Observable<FetchNewsResponse>;
//   }
//   updateNews(createNewsRequest: CreateNewsRequest, fileToUpload: File): Observable<FetchNewsResponse> { // uploadFile
//     const formdata: FormData = new FormData();
//     // console.log( fileToUpload);
//     formdata.append('Image File', fileToUpload);
//     // console.log(formdata.get('imageFile'));
//     formdata.append('News Request', JSON.stringify(createNewsRequest));
//     // console.log(formdata.get('galleryImageRequest'));
//     return this.put(`${this.apiEndpoints.addOrEditNews}`, formdata) as Observable<FetchNewsResponse>;
//   }
//   fetchNews(fetchNewsRequest: FetchNewsRequest): Observable<FetchNewsResponse> { // getAll
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.fetchNews}`, fetchNewsRequest) as Observable<FetchNewsResponse>;
//   }
//   inActivateNews(id: number): Observable<FetchNewsResponse> { // getAll
//     //   // tslint:disable-next-line:no-angle-bracket-type-assertion
//     return this.delete(`${this.apiEndpoints.inactiveNews}/${id}`, id) as Observable<FetchNewsResponse>;
//   }

//   fetchlatestNews(fetchNewsRequest: FetchNewsRequest): Observable<FetchLatetnewsResponse> {
//     return this.post(`${this.apiEndpoints.fetchlatestNews}`, fetchNewsRequest) as Observable<FetchLatetnewsResponse>;
//   }
//   addpartner(addPartnerRequest: AddPartnerRequest, fileToUpload: File): Observable<OfficalpartnerResponse> { // uploadFile
//     const formdata: FormData = new FormData();
//     formdata.append('Image File', fileToUpload);
//     console.log('image' + formdata.get('imageFile'));
//     formdata.append('Official Partner Request', JSON.stringify(addPartnerRequest));
//     console.log('request' + formdata.get('Official Partner Request'));
//     return this.post(`${this.apiEndpoints.addpartners}`, formdata) as Observable<OfficalpartnerResponse>;
//   }
//   fetchpartners(fetchPartnerRequest: FetchPartnerRequest): Observable<OfficalpartnerResponse> { // getAll
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.fetchpartners}`, fetchPartnerRequest) as Observable<OfficalpartnerResponse>;
//   }
//   fetchactivepartners(fetchPartnerRequest: FetchPartnerRequest): Observable<OfficalpartnerResponse> { // getAll
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.fetchactivepartners}`, fetchPartnerRequest) as Observable<OfficalpartnerResponse>;
//   }
//   partnerinActivate(id: number): Observable<OfficalpartnerResponse> { // getAll
//     // tslint:disable-next-line:no-angle-bracket-type-assertion
//     return this.put(`${this.apiEndpoints.partnerinactive}/${id}`, id) as Observable<OfficalpartnerResponse>;
//   }
//   partnerActivate(id: number): Observable<OfficalpartnerResponse> { // getAll
//     // tslint:disable-next-line:no-angle-bracket-type-assertion
//     return this.put(`${this.apiEndpoints.partneractive}/${id}`, id) as Observable<OfficalpartnerResponse>;
//   }
//   updatepartner(addPartnerRequest: AddPartnerRequest, fileToUpload: File): Observable<OfficalpartnerResponse> { // uploadFile
//     const formdata: FormData = new FormData();
//     formdata.append('Image File', fileToUpload);
//     formdata.append('Official Partner Request', JSON.stringify(addPartnerRequest));
//     return this.put(`${this.apiEndpoints.partnerupdate}`, formdata) as Observable<OfficalpartnerResponse>;
//   }
//   fetchEnquiries(fetchEnquiryRequest: FetchEnquiryRequest): Observable<FetchEnquiryResponse> { // getAll
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.fetchEnquiry}`, fetchEnquiryRequest) as Observable<FetchEnquiryResponse>;
//   }
//   fetchOfferEnquires(id: number): Observable<FetchOffersResponse> { // getAll
//     //   // tslint:disable-next-line:no-angle-bracket-type-assertion
//     return this.post(`${this.apiEndpoints.fetchEnquiryOffers}/${id}`, id) as Observable<FetchOffersResponse>;
//   }
//   createarea(areaRequest: AreaRequest): Observable<AreaResponse> {
//     return this.post(`${this.apiEndpoints.createarea}`, areaRequest) as Observable<AreaResponse>;
//   }
//   updatearea(areaRequest: AreaRequest): Observable<AreaResponse> {
//     return this.put(`${this.apiEndpoints.updatearea}`, areaRequest) as Observable<AreaResponse>;
//   }
//   areainactive(id): Observable<AreaResponse> {
//     return this.put(`${this.apiEndpoints.areainactive}/${id}`, id) as Observable<AreaResponse>;
//   }
//   areaactive(id): Observable<AreaResponse> {
//     return this.put(`${this.apiEndpoints.areaactive}/${id}`, id) as Observable<AreaResponse>;
//   }
//   createcategory(createCategoryRequest: CreateCategoryRequest): Observable<CreateCategoryResponse> {
//     return this.post(`${this.apiEndpoints.createcategory}`, createCategoryRequest) as Observable<CreateCategoryResponse>;
//   }
//   updatecategory(createCategoryRequest: CreateCategoryRequest): Observable<CreateCategoryResponse> {
//     return this.put(`${this.apiEndpoints.updatecategory}`, createCategoryRequest) as Observable<CreateCategoryResponse>;
//   }
//   inactivecategory(id): Observable<CreateCategoryResponse> {
//     return this.put(`${this.apiEndpoints.categoryInactive}/${id}`, id) as Observable<CreateCategoryResponse>;
//   }
//   activecategory(id): Observable<CreateCategoryResponse> {
//     return this.put(`${this.apiEndpoints.categoryactive}/${id}`, id) as Observable<CreateCategoryResponse>;
//   }
//   createmanufacture(createManufactureRequest: CreateManufactureRequest): Observable<CreateManufactureResponse> {
//     return this.post(`${this.apiEndpoints.createmanufacture}`, createManufactureRequest) as Observable<CreateManufactureResponse>;
//   }
//   upadatemanufacture(createManufactureRequest: CreateManufactureRequest): Observable<CreateManufactureResponse> {
//     return this.put(`${this.apiEndpoints.manufactureupdate}`, createManufactureRequest) as Observable<CreateManufactureResponse>;
//   }
//   inacivatemanufacture(id): Observable<CreateManufactureResponse> {
//     return this.put(`${this.apiEndpoints.inactivemanufacture}/${id}`, id) as Observable<CreateManufactureResponse>;
//   }
//   acivatemanufacture(id): Observable<CreateManufactureResponse> {
//     return this.put(`${this.apiEndpoints.activemanufacture}/${id}`, id) as Observable<CreateManufactureResponse>;
//   }
//   createorigin(createOriginRequest: CreateOriginRequest): Observable<OriginResponse> {
//     return this.post(`${this.apiEndpoints.createorigin}`, createOriginRequest) as Observable<OriginResponse>;
//   }
//   upadateorigin(createOriginRequest: CreateOriginRequest): Observable<OriginResponse> {
//     return this.put(`${this.apiEndpoints.originupdate}`, createOriginRequest) as Observable<OriginResponse>;
//   }
//   activeorigin(id): Observable<OriginResponse> {
//     return this.put(`${this.apiEndpoints.activateorigin}/${id}`, id) as Observable<OriginResponse>;
//   }
//   inactiveorigin(id): Observable<OriginResponse> {
//     return this.put(`${this.apiEndpoints.inactiveorigin}/${id}`, id) as Observable<OriginResponse>;
//   }
//   fetchAllVehicleType(fetchVehicleTypeRequest: FetchVehicleTypeRequest): Observable<FetchVehicleTypeResponse> { // getAll

//     return this.post(`${this.apiEndpoints.fetchVehicleType}`, fetchVehicleTypeRequest) as Observable<FetchVehicleTypeResponse>;
//   }
//   fetchAllactiveVehicleType(fetchVehicleTypeRequest: FetchVehicleTypeRequest, id): Observable<FetchVehicleTypeResponse> { // getAll

//     return this.post(`${this.apiEndpoints.fetchVehicleType}/${id}`, fetchVehicleTypeRequest) as Observable<FetchVehicleTypeResponse>;
//   }
//   createvechileType(createVehicletypeRequest: CreateVehicletypeRequest, fileToUpload: File): Observable<FetchVehicleTypeResponse> {
//     const formdata: FormData = new FormData();
//     // console.log( fileToUpload);
//     formdata.append('Image File', fileToUpload);
//     // console.log(formdata.get('imageFile'));
//     formdata.append('vehicleTypeRequestStr', JSON.stringify(createVehicletypeRequest));
//     return this.post(`${this.apiEndpoints.createvechileType}`, formdata) as Observable<FetchVehicleTypeResponse>;
//   }
//   updatevechileType(pdateVehicleTypeRequest: UpdateVehicleTypeRequest, fileToUpload: File): Observable<FetchVehicleTypeResponse> {
//     const formdata: FormData = new FormData();
//     // console.log( fileToUpload);
//     formdata.append('Image File', fileToUpload);
//     // console.log(formdata.get('imageFile'));
//     formdata.append('VehicleTypeUpdateRequestStr', JSON.stringify(pdateVehicleTypeRequest));
//     return this.put(`${this.apiEndpoints.upadtevechileType}`, formdata) as Observable<FetchVehicleTypeResponse>;
//   }
//   vechicleTypeactivate(id): Observable<FetchVehicleTypeResponse> {
//     return this.put(`${this.apiEndpoints.vehicleactivate}/${id}`, id) as Observable<FetchVehicleTypeResponse>;
//   }
//   vechicleTypedeactivate(id): Observable<FetchVehicleTypeResponse> {
//     return this.put(`${this.apiEndpoints.vehicledeactivate}/${id}`, id) as Observable<FetchVehicleTypeResponse>;
//   }
//   creatContactMethod(createContactMethodRequest: CreateContactMethodRequest): Observable<FetchContactMethodResponse> {
//     return this.post(`${this.apiEndpoints.creatContactMethod}`, createContactMethodRequest) as Observable<FetchContactMethodResponse>;
//   }
//   updateContactMethod(createContactMethodRequest: CreateContactMethodRequest): Observable<FetchContactMethodResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateContactMethod}`, createContactMethodRequest) as Observable<FetchContactMethodResponse>;
//   }
//   activeContactMethod(id): Observable<FetchContactMethodResponse> {
//     return this.put(`${this.apiEndpoints.activateContact}/${id}`, id) as Observable<FetchContactMethodResponse>;
//   }
//   inactiveContactMethod(id): Observable<FetchContactMethodResponse> {
//     return this.put(`${this.apiEndpoints.inactivateContact}/${id}`, id) as Observable<FetchContactMethodResponse>;
//   }
//   creatBusinessType(createBusinessTypeRequest: CreateBusinessTypeRequest): Observable<FetchBusinessTypeResponse> {
//     return this.post(`${this.apiEndpoints.creatBusinessType}`, createBusinessTypeRequest) as Observable<FetchBusinessTypeResponse>;
//   }
//   updateBusinessType(createBusinessTypeRequest: CreateBusinessTypeRequest): Observable<FetchBusinessTypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateBusinessType}`, createBusinessTypeRequest) as Observable<FetchBusinessTypeResponse>;
//   }
//   activeBusinessType(id): Observable<FetchBusinessTypeResponse> {
//     return this.put(`${this.apiEndpoints.activateBusinessType}/${id}`, id) as Observable<FetchBusinessTypeResponse>;
//   }
//   inactiveBusinessType(id): Observable<FetchBusinessTypeResponse> {
//     return this.put(`${this.apiEndpoints.inactivateBusinessType}/${id}`, id) as Observable<FetchBusinessTypeResponse>;
//   }
//   creatInfluenceType(createInfluenceTypeRequest: CreateInfluenceTypeRequest): Observable<FetchInfluenceTypeResponse> {
//     return this.post(`${this.apiEndpoints.creatInfluenceType}`, createInfluenceTypeRequest) as Observable<FetchInfluenceTypeResponse>;
//   }
//   updateInfluenceType(createBusinessTypeRequest: CreateInfluenceTypeRequest): Observable<FetchInfluenceTypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateInfluenceType}`, createBusinessTypeRequest) as Observable<FetchInfluenceTypeResponse>;
//   }
//   createcustomerType(createCustomerTypeRequest: CreateCustomerTypeRequest): Observable<FetchCustomerTypeResponse> {
//     return this.post(`${this.apiEndpoints.createcustomerType}`, createCustomerTypeRequest) as Observable<FetchCustomerTypeResponse>;
//   }

//   updatecustomerType(createCustomerTypeRequest: CreateCustomerTypeRequest): Observable<FetchCustomerTypeResponse> {
//     return this.put(`${this.apiEndpoints.updatecustomerType}`, createCustomerTypeRequest) as Observable<FetchCustomerTypeResponse>;
//   }
//   inactivateCustomerType(id): Observable<FetchCustomerTypeResponse> {
//     return this.put(`${this.apiEndpoints.inactivateCustomerType}/${id}`, id) as Observable<FetchCustomerTypeResponse>;
//   }
//   activateCustomerType(id): Observable<FetchCustomerTypeResponse> {
//     return this.put(`${this.apiEndpoints.activateCustomerType}/${id}`, id) as Observable<FetchCustomerTypeResponse>;
//   }
//   //         createServicePackage(packageElementIds: number[], packageName: string ): Observable<FetchServicePackageResponse> {

//   activeInfluenceType(id): Observable<FetchInfluenceTypeResponse> {
//     return this.put(`${this.apiEndpoints.activateInfluenceType}/${id}`, id) as Observable<FetchInfluenceTypeResponse>;
//   }
//   inactiveInfluenceType(id): Observable<FetchInfluenceTypeResponse> {
//     return this.put(`${this.apiEndpoints.inactivateInfluenceType}/${id}`, id) as Observable<FetchInfluenceTypeResponse>;
//   }

//   createFunctionCode(createFunctionCodeRequest: CreateFunctionCodeRequest): Observable<FetchFunctionCodeResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createFunctionCode}`, createFunctionCodeRequest) as Observable<FetchFunctionCodeResponse>;
//   }
//   updateFunctionCode(createFunctionCodeRequest: CreateFunctionCodeRequest): Observable<FetchFunctionCodeResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.put(`${this.apiEndpoints.updateFunctionCode}`, createFunctionCodeRequest) as Observable<FetchFunctionCodeResponse>;
//   }
//   activeFunctionCode(id): Observable<FetchFunctionCodeResponse> {
//     return this.put(`${this.apiEndpoints.activateFunctionCode}/${id}`, id) as Observable<FetchFunctionCodeResponse>;
//   }
//   inactiveFunctionCode(id): Observable<FetchFunctionCodeResponse> {
//     return this.put(`${this.apiEndpoints.inactivateFunctionCode}/${id}`, id) as Observable<FetchFunctionCodeResponse>;
//   }
//   createReferenceEmployee(createReferenceEmployeeRequest: CreateReferenceEmployeeRequest): Observable<ReferenceEmployeeResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createReferenceEmployee}`, createReferenceEmployeeRequest) as Observable<ReferenceEmployeeResponse>;
//   }
//   fetchReferenceEmployee(fetchReferenceEmployeeRequest: FetchReferenceEmployeeRequest): Observable<ReferenceEmployeeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchReferenceEmployee}`, fetchReferenceEmployeeRequest) as Observable<ReferenceEmployeeResponse>;
//   }
//   updateReferenceEmployee(createReferenceEmployeeRequest: CreateReferenceEmployeeRequest): Observable<ReferenceEmployeeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateReferenceEmployee}`, createReferenceEmployeeRequest) as Observable<ReferenceEmployeeResponse>;
//   }
//   activeReferenceEmployee(id): Observable<ReferenceEmployeeResponse> {
//     return this.put(`${this.apiEndpoints.activateReferenceEmployee}/${id}`, id) as Observable<ReferenceEmployeeResponse>;
//   }
//   inactiveReferenceEmployee(id): Observable<ReferenceEmployeeResponse> {
//     return this.put(`${this.apiEndpoints.inactivateReferenceEmployee}/${id}`, id) as Observable<ReferenceEmployeeResponse>;
//   }
//   fetchComplaintLevel(fetchComplaintLevelRequest: FetchComplaintLevelRequest): Observable<ComplaintLevelResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchComplaintLevel}`, fetchComplaintLevelRequest) as Observable<ComplaintLevelResponse>;
//   }
//   createComplaintLevel(createComplaintLevelRequest: AddComplaintLevel): Observable<ComplaintLevelResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createComplaintLevel}`, createComplaintLevelRequest) as Observable<ComplaintLevelResponse>;
//   }
//   updateComplaintLevel(createComplaintLevelRequest: AddComplaintLevel): Observable<ComplaintLevelResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateComplaintLevel}`, createComplaintLevelRequest) as Observable<ComplaintLevelResponse>;
//   }
//   activeComplaintLevel(id): Observable<ComplaintLevelResponse> {
//     return this.put(`${this.apiEndpoints.activateComplaintLevel}/${id}`, id) as Observable<ComplaintLevelResponse>;
//   }
//   inactiveComplaintLevel(id): Observable<ComplaintLevelResponse> {
//     return this.put(`${this.apiEndpoints.inactivateComplaintLevel}/${id}`, id) as Observable<ComplaintLevelResponse>;
//   }
//   fetchActiveComplaintLevel(fetchComplaintLevelRequest: FetchComplaintLevelRequest): Observable<ComplaintLevelResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchActiveComplaintLevel}`, fetchComplaintLevelRequest) as Observable<ComplaintLevelResponse>;
//   }
//   fetchServiceType(fetchServiceTypeRequest: FetchServiceTypeRequest): Observable<ServiceTypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchservicetype}`, fetchServiceTypeRequest) as Observable<ServiceTypeResponse>;
//   }
//   fetchActiveServiceType(fetchServiceTypeRequest: FetchServiceTypeRequest): Observable<ServiceTypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchActiveServiceType}`, fetchServiceTypeRequest) as Observable<ServiceTypeResponse>;
//   }
//   createServiceType(createServiceTypeRequest: CreateServiceTypeRequest): Observable<ServiceTypeResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createservicetype}`, createServiceTypeRequest) as Observable<ServiceTypeResponse>;
//   }
//   updateServiceType(createServiceTypeRequest: CreateServiceTypeRequest): Observable<ServiceTypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateservicetype}`, createServiceTypeRequest) as Observable<ServiceTypeResponse>;
//   }
//   activeServiceType(id): Observable<ServiceTypeResponse> {
//     return this.put(`${this.apiEndpoints.activateservicetype}/${id}`, id) as Observable<ServiceTypeResponse>;
//   }
//   inactiveServiceType(id): Observable<ServiceTypeResponse> {
//     return this.put(`${this.apiEndpoints.inactivateservicetype}/${id}`, id) as Observable<ServiceTypeResponse>;
//   }
//   fetchVehicleClass(fetchVehicleClassRequest: FetchVehicleClassRequest): Observable<VehicleClassResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchvehicleclass}`, fetchVehicleClassRequest) as Observable<VehicleClassResponse>;
//   }
//   createVehicleClass(createVehicleClassRequest: CreateVehicleClassRequest): Observable<VehicleClassResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createvehicleclass}`, createVehicleClassRequest) as Observable<VehicleClassResponse>;
//   }
//   updateVehicleClass(createVehicleClassRequest: CreateVehicleClassRequest): Observable<VehicleClassResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updatevehicleclass}`, createVehicleClassRequest) as Observable<VehicleClassResponse>;
//   }
//   activeVehicleClass(id): Observable<VehicleClassResponse> {
//     return this.put(`${this.apiEndpoints.activatevehicleclass}/${id}`, id) as Observable<VehicleClassResponse>;
//   }
//   inactiveVehicleClass(id): Observable<VehicleClassResponse> {
//     return this.put(`${this.apiEndpoints.inactivatevehicleclass}/${id}`, id) as Observable<VehicleClassResponse>;
//   }
//   fetchInsurer(fetchInsurerRequest: FetchInsurerRequest): Observable<InsurerResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchinsurer}`, fetchInsurerRequest) as Observable<InsurerResponse>;
//   }
//   createInsurer(createInsurerRequest: CreateInsurerRequest): Observable<InsurerResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createinsurer}`, createInsurerRequest) as Observable<InsurerResponse>;
//   }
//   updateInsurer(createInsurerRequest: CreateInsurerRequest): Observable<InsurerResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.put(`${this.apiEndpoints.updateinsurer}`, createInsurerRequest) as Observable<InsurerResponse>;
//   }
//   activeInsurer(id): Observable<InsurerResponse> {
//     return this.put(`${this.apiEndpoints.activateinsurer}/${id}`, id) as Observable<InsurerResponse>;
//   }
//   inactiveInsurer(id): Observable<InsurerResponse> {
//     return this.put(`${this.apiEndpoints.inactivateinsurer}/${id}`, id) as Observable<InsurerResponse>;
//   }
//   fetchRoadTest(fetchRoadTesterRequest: FetchRoadTesterRequest): Observable<RoadTesterResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchroadtest}`, fetchRoadTesterRequest) as Observable<RoadTesterResponse>;
//   }
//   createRoadTest(createRoadTesterRequest: CreateRoadTesterRequest): Observable<RoadTesterResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createroadtest}`, createRoadTesterRequest) as Observable<RoadTesterResponse>;
//   }
//   updateRoadTest(createRoadTesterRequest: CreateRoadTesterRequest): Observable<RoadTesterResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.put(`${this.apiEndpoints.updateroadtest}`, createRoadTesterRequest) as Observable<RoadTesterResponse>;
//   }
//   activeRoadTest(id): Observable<RoadTesterResponse> {
//     return this.put(`${this.apiEndpoints.activateroadtest}/${id}`, id) as Observable<RoadTesterResponse>;
//   }
//   inactiveRoadTest(id): Observable<RoadTesterResponse> {
//     return this.put(`${this.apiEndpoints.inactivateroadtest}/${id}`, id) as Observable<RoadTesterResponse>;
//   }
//   fetchPurposeOfRoadTest(fetchPurposeofRoadtestRequestRequest: FetchPurposeofRoadtestRequest): Observable<PurposeofRoadtestResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchpurposeofroaddtest}`, fetchPurposeofRoadtestRequestRequest) as Observable<PurposeofRoadtestResponse>;
//   }
//   createPurposeOfRoadTest(createPurposeofRoadtestRequestRequest: CreatePurposeofRoadtestRequest): Observable<PurposeofRoadtestResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createpurposeofroaddtest}`, createPurposeofRoadtestRequestRequest) as Observable<PurposeofRoadtestResponse>;
//   }
//   updatePurposeOfRoadTest(createPurposeofRoadtestRequestRequest: CreatePurposeofRoadtestRequest): Observable<PurposeofRoadtestResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.put(`${this.apiEndpoints.updatepurposeofroaddtest}`, createPurposeofRoadtestRequestRequest) as Observable<PurposeofRoadtestResponse>;
//   }
//   activePurposeOfRoadTest(id): Observable<PurposeofRoadtestResponse> {
//     return this.put(`${this.apiEndpoints.activatepurposeofroaddtest}/${id}`, id) as Observable<PurposeofRoadtestResponse>;
//   }
//   inactivePurposeOfRoadTest(id): Observable<PurposeofRoadtestResponse> {
//     return this.put(`${this.apiEndpoints.inactivatepurposeofroaddtest}/${id}`, id) as Observable<PurposeofRoadtestResponse>;
//   }
//   // tslint:disable-next-line:max-line-length
//   fetchQualityCheckQuestions(fetchQualityCheckQuestionsRequest: FetchQualityCheckQuestionsRequest): Observable<QualityCheckQuestionsResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchqualitycheckquestions}`, fetchQualityCheckQuestionsRequest) as Observable<QualityCheckQuestionsResponse>;
//   }
//   // tslint:disable-next-line:max-line-length
//   updateQualityCheckQuestionsRequest(createQualityCheckQuestionsRequest: CreateQualityCheckQuestionsRequest): Observable<QualityCheckQuestionsResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.put(`${this.apiEndpoints.updatequalitycheckquestions}`, createQualityCheckQuestionsRequest) as Observable<QualityCheckQuestionsResponse>;
//   }
//   // tslint:disable-next-line:max-line-length
//   createQualityCheckQuestionsRequest(createQualityCheckQuestionsRequest: CreateQualityCheckQuestionsRequest): Observable<QualityCheckQuestionsResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createqualitycheckquestions}`, createQualityCheckQuestionsRequest) as Observable<QualityCheckQuestionsResponse>;
//   }
//   fetchCustomerReview(fetchCustomerReviewRequest: FetchCustomerReview): Observable<CustomerReviewResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchCustomerReview}`, fetchCustomerReviewRequest) as Observable<CustomerReviewResponse>;
//   }
//   createCustomerReview(createCustomerReviewRequest: CreateCustomerReview, fileToUpload: File): Observable<CustomerReviewResponse> {
//     const formdata: FormData = new FormData();
//     formdata.append('Customer Review Request', JSON.stringify(createCustomerReviewRequest));
//     formdata.append('Image File', fileToUpload);
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createCustomerReview}`, formdata) as Observable<CustomerReviewResponse>;
//   }
//   updateCustomerReview(createCustomerReviewRequest: CreateCustomerReview, fileToUpload: File): Observable<CustomerReviewResponse> {
//     const formdata: FormData = new FormData();
//     formdata.append('Customer Review Request', JSON.stringify(createCustomerReviewRequest));
//     formdata.append('Image File', fileToUpload);
//     // tslint:disable-next-line:max-line-length
//     return this.put(`${this.apiEndpoints.updateCustomerReview}`, formdata) as Observable<CustomerReviewResponse>;
//   }
//   activeCustomerReview(id): Observable<CustomerReviewResponse> {
//     return this.put(`${this.apiEndpoints. activateCustomerReview}/${id}`, id) as Observable<CustomerReviewResponse>;
//   }
//   inactiveCustomerReview(id): Observable<CustomerReviewResponse> {
//     return this.put(`${this.apiEndpoints.inactivateCustomerReview}/${id}`, id) as Observable<CustomerReviewResponse>;
//   }
//   fetchactiveCustomerReview(fetchCustomerReviewRequest: FetchCustomerReview): Observable<CustomerReviewResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.activecustomerReview}`, fetchCustomerReviewRequest) as Observable<CustomerReviewResponse>;
//   }
//   createPackageElement(addPackageElementRequest: AddPackageElementRequest): Observable<PackageElementResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.addPackageElement}`, addPackageElementRequest) as Observable<PackageElementResponse>;
//   }
//   updatePackageElement(addPackageElementRequest: AddPackageElementRequest): Observable<PackageElementResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updatePackageElement}`, addPackageElementRequest) as Observable<PackageElementResponse>;
//   }
//   fetchPackageElement(fetchPackageElementRequest: FetchPackageElementRequest): Observable<PackageElementResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchPackageElement}`, fetchPackageElementRequest) as Observable<PackageElementResponse>;
//   }
//   fetchActivePackageElement(fetchPackageElementRequest: FetchPackageElementRequest): Observable<PackageElementResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchActivePackageElement}`, fetchPackageElementRequest) as Observable<PackageElementResponse>;
//   }
//   // tslint:disable-next-line:max-line-length
//   fetchActivePackageElementByVehicleId(fetchPackageElementRequest: FetchPackageElementRequest, vehicleTypeId: number): Observable<PackageElementResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchActivePackageElement}/${vehicleTypeId}`, fetchPackageElementRequest) as Observable<PackageElementResponse>;
//   }
//   activePackageElement(id): Observable<PackageElementResponse> {
//     return this.put(`${this.apiEndpoints.activatePackageElement}/${id}`, id) as Observable<PackageElementResponse>;
//   }
//   inactivePackageElement(id): Observable<PackageElementResponse> {
//     return this.put(`${this.apiEndpoints.inactivatePackageElement}/${id}`, id) as Observable<PackageElementResponse>;
//   }
//   fetchServicePackage(fetchServicePackageRequest: FetchServicePackageRequest): Observable<FetchServicePackageResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchServicePackages}`, fetchServicePackageRequest) as Observable<FetchServicePackageResponse>;
//   }

//   fetchActiveServicePackage(fetchServicePackageRequest: FetchServicePackageRequest): Observable<FetchServicePackageResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchActiveServicePackages}`, fetchServicePackageRequest) as Observable<FetchServicePackageResponse>;
//   }
//   fetchGeneralInspection(fetchGeneralInspectionRequest: FetchGeneralInspectionRequest): Observable<GeneralInspectionResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchGeneralInspection}`, fetchGeneralInspectionRequest) as Observable<GeneralInspectionResponse>;
//   }
//   // tslint:disable-next-line:max-line-length
//   fetchActiveGeneralInspection(fetchGeneralInspectionRequest: FetchGeneralInspectionRequest): Observable<GeneralInspectionResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchActiveGeneralInspection}`, fetchGeneralInspectionRequest) as Observable<GeneralInspectionResponse>;
//   }
//   createGeneralInspection(addGeneralInspection: AddGeneralInspection): Observable<GeneralInspectionResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.addGeneralInspection}`, addGeneralInspection) as Observable<GeneralInspectionResponse>;
//   }
//   updateGeneralInspection(addGeneralInspection: AddGeneralInspection): Observable<GeneralInspectionResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateGeneralInspection}`, addGeneralInspection) as Observable<GeneralInspectionResponse>;
//   }
//   activeGeneralInspection(id): Observable<GeneralInspectionResponse> {
//     return this.put(`${this.apiEndpoints.activateGeneralInspection}/${id}`, id) as Observable<GeneralInspectionResponse>;
//   }
//   inactiveGeneralInspection(id): Observable<GeneralInspectionResponse> {
//     return this.put(`${this.apiEndpoints.inactivateGeneralInspection}/${id}`, id) as Observable<GeneralInspectionResponse>;
//   }

//   fetchTaggedService(fetchTaggedServiceRequest: FetchTaggedServiceRequest): Observable<TaggedServiceResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchTaggedService}`, fetchTaggedServiceRequest) as Observable<TaggedServiceResponse>;
//   }
//   fetchActiveTaggedService(fetchTaggedServiceRequest: FetchTaggedServiceRequest): Observable<TaggedServiceResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchActiveTaggedService}`, fetchTaggedServiceRequest) as Observable<TaggedServiceResponse>;
//   }
//   createTaggedService(addTaggedService: AddTaggedService): Observable<TaggedServiceResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.addTaggedService}`, addTaggedService) as Observable<TaggedServiceResponse>;
//   }
//   updateTaggedService(addTaggedService: AddTaggedService): Observable<TaggedServiceResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateTaggedService}`, addTaggedService) as Observable<TaggedServiceResponse>;
//   }
//   activeTaggedService(id): Observable<TaggedServiceResponse> {
//     return this.put(`${this.apiEndpoints.activateTaggedService}/${id}`, id) as Observable<TaggedServiceResponse>;
//   }
//   inactiveTaggedService(id): Observable<TaggedServiceResponse> {
//     return this.put(`${this.apiEndpoints.inactivateTaggedService}/${id}`, id) as Observable<TaggedServiceResponse>;
//   }
//   createServicePackage(createServicePackageRequest: CreateServicePackageRequest): Observable<FetchServicePackageResponse> {

//     return this.post(`${this.apiEndpoints.addServicePackage}`, createServicePackageRequest) as Observable<FetchServicePackageResponse>;
//   }
//   updateServicePackage(createServicePackageRequest: CreateServicePackageRequest): Observable<FetchServicePackageResponse> {

//     return this.put(`${this.apiEndpoints.updateServicePackage}`, createServicePackageRequest) as Observable<FetchServicePackageResponse>;
//   }
//   activeServicePackage(id): Observable<FetchServicePackageResponse> {
//     return this.put(`${this.apiEndpoints.activateServicePackage}/${id}`, id) as Observable<FetchServicePackageResponse>;
//   }
//   inactiveServicePackage(id): Observable<FetchServicePackageResponse> {
//     return this.put(`${this.apiEndpoints.inactivateServicePackage}/${id}`, id) as Observable<FetchServicePackageResponse>;
//   }
//    fetchUserEnquiries(fetchEnquiryRequest: FetchEnquiryRequest): Observable<FetchRequestedOfferResponse> { // getAll
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.fetchRequestedOffer}`, fetchEnquiryRequest) as Observable<FetchRequestedOfferResponse>;
//   }
//   createComplaintsRepairs(addComplaintsOrRepairsRequest: AddComplaintsOrRepairsRequest): Observable<FetchComplaintsOrRepairsResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.addComplaintOrRepair}`, addComplaintsOrRepairsRequest) as Observable<FetchComplaintsOrRepairsResponse>;
//   }
//   updateComplaintsRepairs(addComplaintsOrRepairsRequest: AddComplaintsOrRepairsRequest): Observable<FetchComplaintsOrRepairsResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updatecomplaintOrRepaire}`, addComplaintsOrRepairsRequest) as Observable<FetchComplaintsOrRepairsResponse>;
//   }
//   inactivateComplaintsRepairs(id: number): Observable<FetchComplaintsOrRepairsResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.inactivatecomplaintOrRepaire}/${id}`, id) as Observable<FetchComplaintsOrRepairsResponse>;
//   }
//   activateComplaintsRepairs(id: number): Observable<FetchComplaintsOrRepairsResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.activatecomplaintOrRepaire}/${id}`, id) as Observable<FetchComplaintsOrRepairsResponse>;
//   }

//    // tslint:disable-next-line: max-line-length
//   fetchcomplaintsRepairs(fetchComplaintsOrRepairstRequest: FetchComplaintsOrRepairstRequest, taggedServiceFilterType: string): Observable<TaggedServiceResponse> {
//    // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchComplaintsByFilter}/${taggedServiceFilterType}`, fetchComplaintsOrRepairstRequest) as Observable<TaggedServiceResponse>;
//   }
//   // fetchActiveComplaintsRepairs(fetchComplaintsOrRepairstRequest: FetchComplaintsOrRepairstRequest): Observable<TaggedServiceResponse> {
//      // tslint:disable-next-line: max-line-length
//   //   return this.post(`${this.apiEndpoints.fetchActiveComplaints}`, fetchComplaintsOrRepairstRequest) as Observable<TaggedServiceResponse>;
//   // }
//   fetchComplaintsRepairs(fetchTaggedServiceRequest: FetchTaggedServiceRequest): Observable<TaggedServiceResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchAllComplaint}`, fetchTaggedServiceRequest) as Observable<TaggedServiceResponse>;
//   }
//   // tslint:disable-next-line:max-line-length
//   userCreate(registrationRequest: RegistrationRequest, fileToUpload: File[] | null, driveFileToUpload: File[] | null, profileFileToUpload: File, signFileToUpload: File, userType: string): Observable<RegistrationResponse> {
//     let params = new HttpHeaders();
//     params = params.append('userType', userType);
//     const formdata: FormData = new FormData();
//     // console.log( fileToUpload);
//     // tslint:disable-next-line: prefer-for-of
//     for ( let i = 0; i < fileToUpload.length; i++) {
//       formdata.append('emiratesIdProofImg', fileToUpload[i]);
//     }
//     // formdata.append('Id Image', fileToUpload);
//     // tslint:disable-next-line: prefer-for-of
//     for ( let i = 0; i < driveFileToUpload.length; i++) {
//       formdata.append('drivingLicenseProofImg', driveFileToUpload[i]);
//     }
//     // formdata.append('License Image', driveFileToUpload);
//     formdata.append('profilePictureImg', profileFileToUpload);
//     formdata.append('signatureImg', signFileToUpload);
//     // console.log(formdata.get('imageFile'));
//     const blob = new Blob([JSON.stringify(registrationRequest)], { type: 'application/json' });
//     formdata.append('userRequest', blob);
//     // formdata.append('userRequest', JSON.stringify(registrationRequest));
//     return this.post(`${this.apiEndpoints.createAdminUser}`, formdata,
//     { mimeType: 'multipart/form-data', headers: params }) as Observable<RegistrationResponse>;
//   }

//   fetchUserByUsername(mobileNumber: string): Observable<RegistrationResponse> {
//     return this.get(`${this.apiEndpoints.user}/${mobileNumber}`, mobileNumber) as Observable<RegistrationResponse>;
//   }
//   addUserAddress(addressCreationRequest: AddressCreationRequest): Observable<AddressResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.addAddress}`, addressCreationRequest) as Observable<AddressResponse>;
//   }
//   updateUserAddress(addressCreationRequest: AddressCreationRequest, addressId: number): Observable<AddressResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.editAddress}/${addressId}`, addressCreationRequest) as Observable<AddressResponse>;
//   }
//    // tslint:disable-next-line:max-line-length
//    userUpdate(updateRegistrationRequest: UpdateRegistrationRequest, fileToUpload: File, driveFileToUpload: File,  profileFileToUpload: File, signFileToUpload: File, userType: string): Observable<RegistrationResponse> {
//     let params = new HttpHeaders();
//     params = params.append('userType', userType);
//     const formdata: FormData = new FormData();
//     // console.log( fileToUpload);
//     formdata.append('Id Image', fileToUpload);
//     formdata.append('License Image', driveFileToUpload);
//     formdata.append('Pro-Pic Image', profileFileToUpload);
//     formdata.append('Signature Image', signFileToUpload);
//     // console.log(formdata.get('imageFile'));
//     formdata.append('User Request', JSON.stringify(updateRegistrationRequest));
//     return this.put(`${this.apiEndpoints.adminEdit}`, formdata, { headers: params }) as Observable<RegistrationResponse>;
//   }
//   updatePassword(updatePasswordRequest: UpdatePasswordRequest): Observable<UpdateAddressResponse> {
//     return this.put(`${this.apiEndpoints.editPassword}`, updatePasswordRequest) as Observable<UpdateAddressResponse>;
//   }
//   fetchallworkcenter(fetchWorkCenterRequest: FetchWorkCenterRequest): Observable<FetchWorkCenterResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchworkcenter}`, fetchWorkCenterRequest) as Observable<FetchWorkCenterResponse>;
//   }
//   activateworkcenter(id: number): Observable<FetchWorkCenterResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.activateworkcenter}/${id}`, id) as Observable<FetchWorkCenterResponse>;
//   }
//   deactivateworkcenter(id: number): Observable<FetchWorkCenterResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.deactivateworkcenter}/${id}`, id) as Observable<FetchWorkCenterResponse>;
//   }
//   createworkcenter(createWorkcenterRequest: CreateWorkcenterRequest): Observable<FetchWorkCenterResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.workcentre}`, createWorkcenterRequest) as Observable<FetchWorkCenterResponse>;
//   }
//   upadateworkcenter(createWorkcenterRequest: CreateWorkcenterRequest): Observable<FetchWorkCenterResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.workcentre}`, createWorkcenterRequest) as Observable<FetchWorkCenterResponse>;
//   }
//   fetchactiveworkcenter(fetchWorkCenterRequest: FetchWorkCenterRequest): Observable<FetchWorkCenterResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.activewrkcenter}`, fetchWorkCenterRequest) as Observable<FetchWorkCenterResponse>;
//   }
//   createsubclass(createSubclassRequest: CreateSubclassRequest): Observable<SubClassResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.createsubclass}`, createSubclassRequest) as Observable<SubClassResponse>;
//   }
//   updatesubclass(createSubclassRequest: CreateSubclassRequest): Observable<SubClassResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.createsubclass}`, createSubclassRequest) as Observable<SubClassResponse>;
//   }
//   fetchallsubclass(fetchSubclassRequest: FetchSubclassRequest): Observable<SubClassResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchallsubclass}`, fetchSubclassRequest) as Observable<SubClassResponse>;
//   }
//   fetchactivesubclass(fetchSubclassRequest: FetchSubclassRequest): Observable<SubClassResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchactivesubclass}`, fetchSubclassRequest) as Observable<SubClassResponse>;
//   }
//   activatesubclass(id: number): Observable<SubClassResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.activatesubclass}/${id}`, id) as Observable<SubClassResponse>;
//   }
//   deactivatesubclass(id: number): Observable<SubClassResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.deactivatesubclass}/${id}`, id) as Observable<SubClassResponse>;
//   }
//   reviewUser(updateUserFlagRequest: UpdateUserFlagRequest): Observable<any> { // getvalid offers
//     // tslint:disable-next-line:max-line-length
//     return this.put(`${this.apiEndpoints.reviewUser}`, updateUserFlagRequest) as Observable<any>;
//   }
//   createBodyStyle(createBodyStyleRequest: CreateBodyStyleRequest): Observable<FetchBodyStyleResponse> {
//     return this.post(`${this.apiEndpoints.createBodyStyle}`, createBodyStyleRequest) as Observable<FetchBodyStyleResponse>;
//   }
//   createCapacityOrCylinders(createCapacityCylinderRequest: CreateCapacityCylinderRequest): Observable<CapacityCylindersResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.post(`${this.apiEndpoints.createCapacityOrCylinder}`, createCapacityCylinderRequest) as Observable<CapacityCylindersResponse>;
//   }
//   fetchCapacityOrCylinders(fetchCapacityCylindersRequest: FetchCapacityCylindersRequest): Observable<CapacityCylindersResponse> {
//     return this.post(`${this.apiEndpoints.allCapacityOrCylinder}`, fetchCapacityCylindersRequest) as Observable<CapacityCylindersResponse>;
//   }
//   fetchBodyStyle(fetchBodyStyleRequest: FetchBodyStyleRequest): Observable<FetchBodyStyleResponse> {
//     return this.post(`${this.apiEndpoints.allBodyStyle}`, fetchBodyStyleRequest) as Observable<FetchBodyStyleResponse>;
//   }

//   createfueltype(createFueltypeRequest: CreateFueltypeRequest): Observable<FetchFueltypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.createfueltype}`, createFueltypeRequest) as Observable<FetchFueltypeResponse>;
//   }

//   updatefueltype(createFueltypeRequest: CreateFueltypeRequest): Observable<FetchFueltypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updatefueltype}`, createFueltypeRequest) as Observable<FetchFueltypeResponse>;
//   }
//   activefueltype(id): Observable<FetchFueltypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.activatefueltype}/${id}`, id) as Observable<FetchFueltypeResponse>;
//   }
//   inactivefueltype(id): Observable<FetchFueltypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.inactivatefueltype}/${id}`, id) as Observable<FetchFueltypeResponse>;
//   }
//   fetchfueltype(fetchFueltypeRequest: FetchFueltypeRequest): Observable<FetchFueltypeResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchfueltype}`, fetchFueltypeRequest) as Observable<FetchFueltypeResponse>;
//   }
//   createhorsepower(createHorsepowerRequest: CreateHorsepowerRequest): Observable<FetchHorsepowerResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.createhorsepower}`, createHorsepowerRequest) as Observable<FetchHorsepowerResponse>;
//   }
//   updatehorsepower(createHorsepowerRequest: CreateHorsepowerRequest): Observable<FetchHorsepowerResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updatehorsepower}`, createHorsepowerRequest) as Observable<FetchHorsepowerResponse>;
//   }
//   fetchhorsepower(fetchHorsepowerRequest: FetchHorsepowerRequest): Observable<FetchHorsepowerResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchhorsepower}`, fetchHorsepowerRequest) as Observable<FetchHorsepowerResponse>;
//   }
//   activehorsepower(id): Observable<FetchHorsepowerResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.activatehorsepower}/${id}`, id) as Observable<FetchHorsepowerResponse>;
//   }
//   inactivehorsepower(id): Observable<FetchHorsepowerResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.inactivatehorsepower}/${id}`, id) as Observable<FetchHorsepowerResponse>;
//   }

//   createtransmission(createTransmissionRequest: CreateTransmissionRequest): Observable<FetchTransmissionResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.createtransmission}`, createTransmissionRequest) as Observable<FetchTransmissionResponse>;
//   }
//   updatetransmission(createTransmissionRequest: CreateTransmissionRequest): Observable<FetchTransmissionResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updatetransmission}`, createTransmissionRequest) as Observable<FetchTransmissionResponse>;
//   }
//   fetchtransmission(fetchTransmissionRequest: FetchTransmissionRequest): Observable<FetchTransmissionResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchtransmission}`, fetchTransmissionRequest) as Observable<FetchTransmissionResponse>;
//   }
//   inactivetransmission(id): Observable<FetchTransmissionResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.inactivetransmission}/${id}`, id) as Observable<FetchTransmissionResponse>;
//   }
//   activetransmission(id): Observable<FetchTransmissionResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.activetransmission}/${id}`, id) as Observable<FetchTransmissionResponse>;
//   }


//   // createtransmission: `transmission/create`,
//   // updatetransmission: `transmission/update`,
//   // fetchtransmission: `transmission/fetch`,
//   // activetransmission: `transmission/activate`,
//   // inactivetransmission: `transmission/inactivate`
//   updateBodyStyle(createBodyStyleRequest: CreateBodyStyleRequest): Observable<FetchBodyStyleResponse> {
//     return this.put(`${this.apiEndpoints.createBodyStyle}`, createBodyStyleRequest) as Observable<FetchBodyStyleResponse>;
//   }
//   updateCapacityOrCylinders(createCapacityCylinderRequest: CreateCapacityCylinderRequest): Observable<CapacityCylindersResponse> {
//     // tslint:disable-next-line:max-line-length
//     return this.put(`${this.apiEndpoints.createCapacityOrCylinder}`, createCapacityCylinderRequest) as Observable<CapacityCylindersResponse>;
//   }
//   activateBodyStyle(id: number): Observable<FetchBodyStyleResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.activateBodyStyle}/${id}`, id) as Observable<FetchBodyStyleResponse>;
//   }
//   inactivateBodyStyle(id: number): Observable<FetchBodyStyleResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.inactivateBodyStyle}/${id}`, id) as Observable<FetchBodyStyleResponse>;
//   }
//   activateCapacityOrCylinders(id: number): Observable<CapacityCylindersResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.activateCapacity}/${id}`, id) as Observable<CapacityCylindersResponse>;
//   }
//   inactivateCapacityOrCylinders(id: number): Observable<CapacityCylindersResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.inactivateCapacity}/${id}`, id) as Observable<CapacityCylindersResponse>;
//   }
//   updateUserAddressNumber(updateAddressNumberRequest: UpdateAddressNumberRequest): Observable<RegistrationResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateAddressNumber}`, updateAddressNumberRequest) as Observable<RegistrationResponse>;
//   }
//   updateAssetNumber(vehicleId: number, assetNumber: string): Observable<VehicleRegistrationResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.put(`${this.apiEndpoints.updateAssetNumber}/${vehicleId}/${assetNumber}`, vehicleId, assetNumber) as Observable<VehicleRegistrationResponse>;
//   }
//   checkMobileNum(mobileNumber: string): Observable<ExistingUserCheck> {
//     return this.get(`${this.apiEndpoints.mobNumCheck}/${mobileNumber}`, mobileNumber) as Observable<ExistingUserCheck>;
//   }

//   createvehiclecolor(vehiclecolorRequest: VehiclecolorRequest): Observable<VehicleColorResponse> {
//     return this.post(`${this.apiEndpoints.createvehiclecolor}`, vehiclecolorRequest) as Observable<VehicleColorResponse>;
//   }
//   updatevehiclecolor(vehiclecolorRequest: VehiclecolorRequest): Observable<VehicleColorResponse> {
//     return this.put(`${this.apiEndpoints.updatevehiclecolor}`, vehiclecolorRequest) as Observable<VehicleColorResponse>;
//   }
//   vehiclecolorinactive(id): Observable<VehicleColorResponse> {
//     return this.put(`${this.apiEndpoints.vehiclecolorinactive}/${id}`, id) as Observable<VehicleColorResponse>;
//   }
//   vehiclecoloractive(id): Observable<VehicleColorResponse> {
//     return this.put(`${this.apiEndpoints.vehiclecoloractive}/${id}`, id) as Observable<VehicleColorResponse>;
//   }
//   fetchActiveVehicleColor(fetchHorsepowerRequest: FetchHorsepowerRequest): Observable<ActiveVehicleColorResponse> {
//     // tslint:disable-next-line: max-line-length
//     return this.post(`${this.apiEndpoints.fetchActiveVehColor}`, fetchHorsepowerRequest) as Observable<ActiveVehicleColorResponse>;
//   }
//   fetchUerTypeUser(fetchUserUserType: FetchUserUserType): Observable<UserDetailsResponse> {
//     return this.post(`${this.apiEndpoints.fetchUserTypeUser}`, fetchUserUserType) as Observable<UserDetailsResponse>;
// }
// fetchRequestToDepartment(id) : Observable<DepartmentRequestPayload> {
//   return this.get(`${this.apiEndpoints.fetchrequestToDepartments}/${id}`, id) as Observable<DepartmentRequestPayload>;
// }
// requestToDepartments(requestToDepartments: RequestToDeptRequest): Observable<DepartmentRequestPayload> {
//   // tslint:disable-next-line: max-line-length
//   return this.post(`${this.apiEndpoints.fetchrequestToDepartments}`, requestToDepartments) as Observable<DepartmentRequestPayload>;
// }
}
