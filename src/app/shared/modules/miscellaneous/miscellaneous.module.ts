import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faGlobe, faToolbox } from '@fortawesome/free-solid-svg-icons';
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons';
import { faSquare as farSquare, faCheckSquare as farCheckSquare } from '@fortawesome/free-regular-svg-icons';
import { faStackOverflow, faGithub, faMedium } from '@fortawesome/free-brands-svg-icons';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ImageCropperModule } from 'ngx-image-cropper';
import { MatTableExporterModule } from 'mat-table-exporter';
import { CommonModule } from '@angular/common';
// https://github.com/FortAwesome/angular-fontawesome
// https://fontawesome.com/how-to-use/on-the-web/referencing-icons/basic-use


@NgModule({
  declarations: [],
  imports: [
    CommonModule ,
    FlexLayoutModule,
    FontAwesomeModule,
    // NgxAudioPlayerModule,
    NgMultiSelectDropDownModule,
    ImageCropperModule,
    MatTableExporterModule,
  ],
  exports: [FlexLayoutModule, FontAwesomeModule,  
    //  NgxAudioPlayerModule,
     NgMultiSelectDropDownModule,
     ImageCropperModule,  MatTableExporterModule
  ]
})
// https://stackblitz.com/edit/angular-fontawesome-icon-library?file=src%2Fapp%2Fapp.component.html
export class MiscellaneousModule {
  // constructor(private library: FaIconLibrary) {
  //   library.addIcons(faGlobe, faToolbox, faCheckSquare, farSquare, farCheckSquare, faStackOverflow, faGithub, faMedium);
  // }
}
