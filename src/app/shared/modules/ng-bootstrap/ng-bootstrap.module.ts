import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule, NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    NgbModule
  ],
  declarations: [],
  exports: [NgbModule],
  providers: [NgbActiveModal]
})
export class NgBootstrapModule { }
