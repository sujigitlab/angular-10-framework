import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppLoginComponent } from './app-login/app-login.component';
import { AppPageNotFoundComponent } from './app-page-not-found/app-page-not-found.component';
import { AppAccessDeniedComponent } from './app-access-denied/app-access-denied.component';
import { AppPageLoaderComponent } from './app-page-loader/app-page-loader.component';
import { AppEnvCheckComponent } from './app-env-check/app-env-check.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { HomeComponent } from './views/home/home.component';
import { SiteContentLayoutComponent } from './views/layout/site-content-layout/site-content-layout.component';
import { SiteContentComponent } from './views/layout/site-content-layout/site-content/site-content.component';
import { SiteHeaderComponent } from './views/layout/site-header/site-header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { UserIdleModule } from 'angular-user-idle';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AppLoginComponent,
    AppPageNotFoundComponent,
    AppAccessDeniedComponent,
    AppPageLoaderComponent,
    AppEnvCheckComponent,
    SiteContentLayoutComponent,
    SiteContentComponent,
    SiteHeaderComponent,
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    CoreModule,
    SharedModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    UserIdleModule.forRoot({idle: 600, timeout: 300, ping: 290}),
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
