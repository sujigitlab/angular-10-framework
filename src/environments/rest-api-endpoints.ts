export const apiEndpoints = {
    'api-1': {
      userRegister: `user/create`,
      userLogin: `login/otp`,
      logout: `logout`,
      refreshToken: `refresh`,
      otp: 'otp',
      createUser: 'user/create2',
      login: 'login',
      password: `password`
},

    'api-2': {
        sampleSearch: 'serviceapp/api/contextpat/v1/functionality/search'
    }
};


